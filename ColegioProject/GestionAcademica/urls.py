from django.conf.urls import url, include
from GestionAcademica import views

urlpatterns=[
    url(r'^$', views.index, name="index" ),
    url(r'^crearAlumno$', views.crearAlumno, name="crearAlumno" ),
    url(r'^listarAlumnos$', views.listarAlumnos, name="listarAlumnos" ),
    url(r'^actualizarAlumno(?P<id>\d+)/$', views.actualizarAlumno, name="actualizarAlumno" ),
    url(r'^eliminarAlumno(?P<id>\d+)/$', views.eliminarAlumno, name="eliminarAlumno" ),
]
