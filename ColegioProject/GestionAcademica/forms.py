from django import forms
from GestionAcademica import models

#Formulario alumno
class AlumnoForm(forms.ModelForm):
    cedula = forms.CharField(label='Cedula',required=True, widget=forms.TextInput(attrs={'placeholder': 'Cedula'}))
    nombres = forms.CharField(label='Nombres',required=True, widget=forms.TextInput(attrs={'placeholder': 'Nombres'}))
    apellido_paterno = forms.CharField(label='Apellido paterno',required=True, widget=forms.TextInput(attrs={'placeholder': 'Apellido paterno'}))
    apellido_materno = forms.CharField(label='Apellido materno',required=True, widget=forms.TextInput(attrs={'placeholder': 'Apellido materno'}))
    fecha_nacimiento = forms.DateField(label="Fecha de inicio", required=True)
    SEXOS_CHOICES = (
        ('F', 'Femenino'),
        ('M', 'Masculino')
    )
    sexo = forms.ChoiceField(widget=forms.RadioSelect, choices=SEXOS_CHOICES)

    class Meta:
        model = models.Alumno
        fields = ('cedula', 'nombres', 'apellido_paterno', 'apellido_materno', 'fecha_nacimiento', 'sexo')
