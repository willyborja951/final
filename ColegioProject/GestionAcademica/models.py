from django.db import models


# Create your models here.

class Alumno(models.Model):
    cedula = models.CharField(max_length=10)
    nombres = models.CharField(max_length=35)
    apellido_paterno = models.CharField(max_length=35)
    apellido_materno = models.CharField(max_length=35)
    fecha_nacimiento = models.DateField()
    SEXOS_CHOICES = (
        ('F', 'Femenino'),
        ('M', 'Masculino')
    )
    sexo = models.CharField(max_length=2, choices=SEXOS_CHOICES, default='M')

    def nombre_completo(self):
        return self.apellido_paterno + " " + self.apellido_materno + ", " + self.nombres

    def __str__(self):
        return self.nombre_completo()


class Curso(models.Model):
    nombre = models.CharField(max_length=50)
    estado = models.BooleanField(default=True)

    def __str__(self):
        return self.nombre


class Matricula(models.Model):
    alumno = models.ForeignKey(Alumno, null=False, blank=False, on_delete=models.CASCADE)
    curso = models.ForeignKey(Curso, null=False, blank=False, on_delete=models.CASCADE)
    fecha_matricula = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.alumno.nombre_completo() + " - " + self.curso.nombre
