from django.shortcuts import render
from GestionAcademica import forms, models
from django.http import HttpResponseRedirect
from django.urls import reverse

# Create your views here.
#Index
def index(request):
    return render (request, "index.html")

def crearAlumno(request):
    if request.method == 'POST':
        formularioCrearAlumno = forms.AlumnoForm(request.POST, prefix="formularioCrearAlumno")
        if formularioCrearAlumno.is_valid():
            cedula = formularioCrearAlumno.cleaned_data.get('cedula')
            nombres = formularioCrearAlumno.cleaned_data.get('nombres')
            apellido_materno = formularioCrearAlumno.cleaned_data.get('apellido_materno')
            apellido_paterno = formularioCrearAlumno.cleaned_data.get('apellido_paterno')
            fecha_nacimiento = formularioCrearAlumno.cleaned_data.get('fecha_nacimiento')
            sexo = formularioCrearAlumno.cleaned_data.get('sexo')
            nuevoAlumno = models.Alumno(cedula = cedula, nombres = nombres, apellido_materno = apellido_materno, apellido_paterno = apellido_paterno, fecha_nacimiento = fecha_nacimiento, sexo = sexo)
            nuevoAlumno.save()
            return HttpResponseRedirect(reverse('listarAlumnos'))
    else:
        formularioCrearAlumno = forms.AlumnoForm(prefix="formularioCrearAlumno")
    return render (request, "formularioCrearAlumno.html", {'formularioCrearAlumno':formularioCrearAlumno})

def listarAlumnos(request):
    alumnosRegistrados = models.Alumno.objects.filter()
    return render (request, "formularioListarAlumnos.html", {'alumnosRegistrados':alumnosRegistrados})

def actualizarAlumno(request, id):
    if request.method == 'POST':
        formularioActualizarAlumno = forms.AlumnoForm(request.POST, prefix="formularioActualizarAlumno")
        if formularioActualizarAlumno.is_valid():
            cedula = formularioActualizarAlumno.cleaned_data.get('cedula')
            nombres = formularioActualizarAlumno.cleaned_data.get('nombres')
            apellido_materno = formularioActualizarAlumno.cleaned_data.get('apellido_materno')
            apellido_paterno = formularioActualizarAlumno.cleaned_data.get('apellido_paterno')
            fecha_nacimiento = formularioActualizarAlumno.cleaned_data.get('fecha_nacimiento')
            sexo = formularioActualizarAlumno.cleaned_data.get('sexo')
            alumno = models.Alumno.objects.get(id=id)
            alumno.cedula = cedula
            alumno.nombres = nombres
            alumno.apellido_materno = apellido_materno
            alumno.apellido_paterno = apellido_paterno
            alumno.fecha_nacimiento = fecha_nacimiento
            alumno.sexo = sexo
            alumno.save()
            print(alumno,cedula)
        return HttpResponseRedirect(reverse('listarAlumnos'))

    else:
        alumno = models.Alumno.objects.get(id=id)
        formularioActualizarAlumno = forms.AlumnoForm(instance = alumno)
        return render (request, "formularioActualizarAlumno.html", {'formularioActualizarAlumno':formularioActualizarAlumno})

def eliminarAlumno(request, id):
    alumno = models.Alumno.objects.get(id=id)
    alumno.delete()
    return HttpResponseRedirect(reverse('listarAlumnos'))
